# Virtualna produkcija u Unreal Engine 4 razvojnom okruženju

Projekt R na preddiplomskom studiju Fakulteta elektrotehnike i računarstva ak.god. 2020./2021. pod voditeljstvom mentora izv. prof. dr. sc. Lee Skorin-Kapov i doc. dr. sc. Mirka Sužnjevića.

Tema projekta jest demonstracija povezivanja stvarne kamere u fizičkom prostoru te kamere u virtualnom svijetu unutar scene stvorene unutar _Unreal Engine 4_ pogonskog sklopa igre. Na taj način postiže se kontrola perspektive virtualne kamere pomoću stvarne u stvarnom vremenu.

Podaci o položaju fizičke kamere u prostoru određuju se pomoću jednog _HTC Vive Trackera_ i dva _Base Stationa_ te se tako prikupljeni podaci u stvarnom vremenu šalju u stvoreni sustav unutar Unreala i obrađuju na prikladan način kako bi se oponašali pokreti stvarne kamere putem virtualne kamere smještene unutar UE4 scene.

Cilj projekta stvaranje je sustava koji uspješno mapira pokrete kamere iz stvarnog svijeta u virtualni, u svrhu mogućnosti snimanja nekog objekta u stvarnom životu ispred LED zaslona i postavljanja istog unutar virtualnog u sklopu videa – na taj način povezati dobivene infomacije u stvarnom vremenu i uspješno smjestiti snimani objekt u dinamičku virtualnu scenu putem snimljenog videa ispred LED zaslona.

[Video demonstracije projekta](https://www.youtube.com/watch?v=NEyAkUCLPCs&feature=emb_imp_woyt)
